@extends('layouts.app')

@section('content')
    <table class="table table-striped table-dark">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Foto</th>
                <th scope="col">Username</th>
                <th scope="col">Texto</th>
                <th scope="col">Data</th>
            </tr>
        </thead>
        <tbody>
            @foreach($comments as $comment)
            <tr>
                <th scope="row">{{ $comment->id }}</th>
                <td><img style="border-radius:50%;" src="{{ $comment->profile_picture}}" width="50" height="50"></td>
                <td><a href="https://www.instagram.com/{{ $comment->username }}">{{ "@".$comment->username }}</a></td>
                <td>{{ $comment->comment_text }}</td>
                <td>{{ Carbon\Carbon::parse($comment->commented_at)->format('d/m/Y H:i:s') }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

    {{ $comments->links() }}
@endsection
