<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{route('posts.index')}}">
                    Instagram
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item {{ (request()->is('accounts*')) ? 'active' : '' }}">
                            <a class="nav-link" href="{{route('accounts.index')}}">
                                <i class="fa fa-user"></i> Contas
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ (request()->is('posts*')) ? 'active' : '' }}" href="{{route('posts.index')}}">
                                <i class="fa fa-shopping-cart"></i> Posts
                            </a>
                        </li>

                        
                    </ul>

                   
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <footer class="page-footer font-small bg-dark" style="color:white;">
        <div class="footer-copyright text-center py-3">
        <!-- <img src="https://www.diariodaregiao.com.br/images/icone.png" style="margin-right:10px;"> 
            Diário da Região | Telemarketing
        </div> -->
    </footer>
    <style>
        footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 61px;
        }
        html {
            position: relative;
            min-height: 100%;
        }

        body {
            margin-bottom: 61px;
        }

        /* td {
            white-space: -o-pre-wrap; 
            word-wrap: break-word;
            white-space: pre-wrap; 
            white-space: -moz-pre-wrap; 
            white-space: -pre-wrap; 
        }

        table { 
            table-layout: fixed;
            width: 100%
        } */
        </style>
</body>
</html>
