<!DOCTYPE HTML>
<html>
<head>
    <title>Inserção de dados - Aqui tem Prêmios!</title>
    <?php $postCode = explode('/',$_SERVER['REDIRECT_URL']); ?>
    <?php $postCode = end($postCode); ?>
    <meta http-equiv="refresh" content="<?php echo $_GET['time']; ?>;url=<?php echo URL::to("/getComments/$postCode"); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        html, body {
            height: 100%;
            font-family: 'Arial';
        }
        body {
            margin: 0;
        }
        .flex-container {
            height: 100%;
            padding: 0;
            margin: 0;
            display: -webkit-box;
            display: -moz-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .row {
            width: auto;
            border: 1px solid blue;
            padding:30px;
        }
        .flex-item {
            width: 100%;
            margin: 10px;
            line-height: 20px;
            font-weight: bold;
            font-size: 2em;
            text-align: center;
        }
    </style>
</head>
<body>

<div class="flex-container">
    <div class="row"> 
        <div class="flex-item">
            <img src="https://www.instagram.com/static/images/ico/xxhdpi_launcher.png/99cf3909d459.png">
            <br><br>
            <div>Próxima inserçao em <span id="time"> 10:00</span> minutos!</div>
        </div>
    </div>
</div>


<script>
function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10);
            seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;

        if (--timer < 0) {
            timer = duration;
        }
    }, 1000);
}

window.onload = function () {
    var fiveMinutes = <?= $_GET['time']; ?>,
        display = document.querySelector('#time');
    startTimer(fiveMinutes, display);
};
</script>

</body>
</html>
