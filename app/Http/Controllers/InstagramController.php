<?php

namespace App\Http\Controllers;

set_time_limit(0);

use Illuminate\Http\Request;

use Phpfastcache\Helper\Psr16Adapter;

use App\Comment;
use App\Account;
use App\Error;
use App\Post;
use Illuminate\Support\Facades\Crypt;


class InstagramController extends Controller
{
    private function accountReset(){
        $accounts = Account::all()->count();
        $in_use = Account::where('in_use', 1)->get()->count();
        if($in_use == $accounts){
            $allAccounts = Account::all();
            foreach($allAccounts as $oneAccount){
                $oneAccount->update(['in_use' => 0]);
            }
        }
    }

    private function decryptHash($hash)
    {
        if (empty($hash)) return [];

        $decrypted = Crypt::decryptString($hash); // array

        $result = explode('..', $decrypted);
        $data = [
            'email'     => $result[0] ?? '',
            'password'  => $result[1] ?? '',
            'date'      => $result[2] ?? ''
        ];

        return $data['password'];
    }
    
    // PostCode = CK7TBeFl80i
    public function index($postCode) {
        // Verifica as contas em uso, se todas estiverem em uso, zera
        $post = Post::where('instagram_post_code', $postCode)->first();

        $instagramAccount = Account::where('in_use', 0)->first();
        $instagramAccount->update(['in_use' => 1]);
        $this->accountReset();

        $username = $instagramAccount['email'];
        $password = $this->decryptHash($instagramAccount['password']);
        
        $lastId = $post['max_id'] ?? false;

        // If proxy is needed
        // $instagram = new \InstagramScraper\Instagram(new \GuzzleHttp\Client(['proxy' => 'tcp://localhost:8125']));
        $instagram = \InstagramScraper\Instagram::withCredentials(new \GuzzleHttp\Client(), $username, $password, new Psr16Adapter('Files'));
        try {
            $instagram->login(); // will use cached session if you want to force login $instagram->login(true)
            $instagram->saveSession(); //DO NOT forget this in order to save the session, otherwise have no sense
            $insert_data = [];

            // Get media comments by shortcode
            $response = $instagram->getPaginateMediaCommentsByCode($postCode, $post['count'], $lastId);

            if( $response == 'exception' )
                return redirect("/wait/$postCode?time=120");

            $post->update(['max_id' => $response->maxId]);
            
            foreach($response->comments as $comment) {
                $comment_exist = Comment::where('comment_id', $comment->getId())
                    ->where('post_id', $post->id)
                    ->first();
                
                if( !$comment_exist ) {
                    $account = $comment->getOwner();
                    $commentInstagram = [
                        'post_code'         => $postCode,
                        'post_id'           => $post->id,
                        'comment_id'        => $comment->getId(),
                        'commented_at'      => date('Y-m-d H:i:s', $comment->getCreatedAt()),
                        'comment_text'      => $comment->getText(),
                        'user_id'           => $account->getId(),
                        'username'          => $account->getUsername(),
                        'profile_picture'   => $account->getProfilePicUrl(),
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ];

                    $insert_data[] = $commentInstagram;
                }
            }

            $insert_data = collect($insert_data);
            $chunks      = $insert_data->chunk(1000);

            foreach ($chunks as $chunk) {
                \DB::table('comments')->insert($chunk->toArray());
            }
            //


            return redirect("/wait/$postCode?time=".$post['sleep_time']);
        } catch (Exception $e) {
            Error::create([
                'exception' => $e->getMessage(),
                'post_id' => $postCode
            ]);

            echo $e->getMessage() . "\n";
        }
    }

    public function waitPage() {
        return view('wait');
    }

    public function getAllComments($postCode) {
        $comments = Comment::where('post_code', $postCode)->paginate();
        return view('comments', ['comments' => $comments]);
    }
}
