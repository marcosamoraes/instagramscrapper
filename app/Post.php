<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'instagram_post_code',
        'sleep_time',
        'count',
        'total',
        'max_id',
        'paginateInd'
    ];

    public function comments()
    {
        return $this->hasMany(Comment::class, 'post_id','id');
    }
}
