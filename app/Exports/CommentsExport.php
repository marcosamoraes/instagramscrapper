<?php 

namespace App\Exports;

use App\Comment;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;

class CommentsExport implements FromQuery
{
    use Exportable;

    public function __construct(int $post_id)
    {
        $this->post_id = $post_id;
    }

    public function query()
    {
        return Comment::query()->where('post_id', $this->post_id);
    }
}