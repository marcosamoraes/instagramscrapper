<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/getComments/{postCode}', 'InstagramController@index')->name('get.comments');
Route::get('/wait/{postCode}', 'InstagramController@waitPage');
Route::get('/comments/{postCode}', 'InstagramController@getAllComments')->name('read.comments');
Route::get('/comments-post/export/{postId}', 'PostController@export')->name('export.comments');

Route::resource('posts', 'PostController');
// Route::resource('comments', 'CommentController');
Route::resource('accounts', 'AccountController');
Route::resource('errors', 'ErrorController');